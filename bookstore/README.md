# 圖書館 API 服務

練習項目，模擬書店的圖書管理後端服務

- API集合
 
    > /book
  
    - post 
      - 創建一筆新的圖書資料
    - get  
      - 返回所有圖書資料
 
    > /book/{id}
  
    - post   
      - 更新特定圖書資料
    - get
      - 返回特定圖書資料
    - delete
      - 刪除特定圖書資料
  
## logic diagram

- ref by the photo below

    ![img.png](assets/img.png)
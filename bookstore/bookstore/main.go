package main

import (
	_ "bookstore/internal/store"
	"bookstore/server"
	"bookstore/store/factory"
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	s, err := factory.New("mem") // 創建圖書館存儲模組實例
	if err != nil {
		panic(err)
	}
	service := server.NewBookStoreServer(":8080", s)

	errChan, err := service.ListenAndServe()
	if err != nil {
		log.Println("web server started fail", err)
		return
	}
	log.Println("web server starting...")

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	select { //監聽來自errChan和c的事件
	case err = <-errChan:
		log.Println("web server run fail", err)
		return
	case <-c:
		log.Println("bookstore program is exiting...")
		ctx, cf := context.WithTimeout(context.Background(), time.Second)
		defer cf()
		err = service.Shutdown(ctx) // 關閉http服務實例
	}
	if err != nil {
		log.Println("bookstore program exit error", err)
		return
	}
	log.Println("bookstore program exit ok")
}

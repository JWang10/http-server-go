package store

import (
	mystore "bookstore/store"
	factory "bookstore/store/factory"
	"sync"
)

func init() {
	factory.Register("mem", &MemStore{
		books: make(map[string]*mystore.Book),
	})
}

type MemStore struct {
	sync.RWMutex
	books map[string]*mystore.Book // 提供基於map的Store接口
}

// Create a new Book in the store
func (m MemStore) Create(book *mystore.Book) error {
	m.Lock()
	defer m.Unlock()

	if _, ok := m.books[book.Id]; ok {
		return mystore.ErrExist
	}

	//nbook := *book // 應該給予低層存儲區塊位址，使得資料有正確存入，可以讀取
	m.books[book.Id] = &*book //&nbook
	return nil
}

// Update the existed book in the store
func (m MemStore) Update(book *mystore.Book) error {
	m.Lock()
	defer m.Unlock()

	oldbook, ok := m.books[book.Id]
	if !ok {
		return mystore.ErrNotFound
	}

	nbook := *oldbook
	if nbook.Name != "" {
		nbook.Name = book.Name
	}
	if nbook.Authors != nil {
		nbook.Authors = book.Authors
	}
	if nbook.Press != "" {
		nbook.Press = book.Press
	}
	m.books[book.Id] = &nbook
	return nil
}

// Get retrieves a book from the store by id. If no id exists,
// error is returned.
func (m MemStore) Get(id string) (mystore.Book, error) {
	m.RLock()
	defer m.RUnlock()

	if book, ok := m.books[id]; ok {
		return *book, nil
	}
	return mystore.Book{}, mystore.ErrNotFound
}

// GetAll retrieves all books from the store in arbitrary order.
// If the store is empty, error is returned
func (m MemStore) GetAll() ([]mystore.Book, error) {
	m.RLock()
	defer m.RUnlock()

	allBooks := make([]mystore.Book, 0, len(m.books))
	for _, book := range m.books {
		allBooks = append(allBooks, *book)
	}
	return allBooks, nil
}

func (m MemStore) Delete(id string) error {
	m.Lock()
	defer m.Unlock()

	if _, ok := m.books[id]; !ok {
		return mystore.ErrNotFound
	}

	delete(m.books, id)
	return nil
}

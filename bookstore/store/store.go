package store

import "errors"

var (
	ErrNotFound = errors.New("not found")
	ErrExist    = errors.New("exist")
)

type Book struct {
	Id      string   `json:"id"` // ISBN ID
	Name    string   `json:"name"`
	Authors []string `json:"authors"`
	Press   string   `json:"press"` //出版社
}
type Store interface {
	Create(book *Book) error
	Update(book *Book) error
	Get(string) (Book, error)
	GetAll() ([]Book, error)
	Delete(string) error
}

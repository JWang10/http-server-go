package main

import (
	"fmt"
	"io"
	"net/http"
)

func main() {
	h1 := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "hello from handleFunc #1\n")
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello, World"))
	})
	http.HandleFunc("/h1", h1)
	port := 8080
	fmt.Println("Server starting with port:", port)
	http.ListenAndServe(":8080", nil)
	//log.Fatalln(http.ListenAndServe(":8080", nil))
}
